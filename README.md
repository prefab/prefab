# Prefab

Framework for automating the deployment of software.

Configuration is done in the programming language that Prefab is written in (Scala).  
This has the following advantages:
- Auto-completion of available configuration options (in appropriate editors)
- Compile-time checks to catch configuration problems without having to run a deployment (and in appropriate editors, you'll get feedback as you type)
- Writing a configurable module is similar to writing configuration


## Usage

Please mind that this project is still in an early stage of development.
You probably don't want to actually use this yet for your own deployments.


### Configuration

This is how you would configure your home-server to have `tree` installed:

```scala
import prefab.prelude.config.*
import prefab.tech.pkg.packages.*

@main def main() = deploy { deployment =>
	val homeServer = deployment.host("home-server")
	
	homeServer.ensure(Packages.Installed("tree"))
}
```

Now, say you wrote tons of logic for configuring your server and want to extract that into a separate file:

```scala
import prefab.prelude.plan.*
import prefab.tech.pkg.packages.*

case class ServerSetup(name: String) extends Plan {
	def ensure(host: LocalHost) = {
		host.ensure(Packages.Installed("tree", "htop"))
		
		host.userDir.file("hello.txt")
			.ensure(_.textIs(s"Hello, $name!"))
	}
}
```
```scala
import prefab.prelude.config.*

@main def main() = deploy { deployment =>
        val homeServer = deployment.host("home-server")

        homeServer.ensure(ServerSetup(name="World"))
}

```

### Deployment

To apply a configuration on a host, you need to run your configuration program on the target host.
So, either build a JAR-file on your dev machine and upload it to the target machine or compile the code on the target.

You can define the configuration for many hosts within one program. Which one will actually be applied to the current host is decided based on the hostname. In the example above, the supplied hostname is "host-server".

