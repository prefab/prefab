package prefab.util


object Formatting {
	
	extension (string: String) {
		
		def indent(level: Int): String = {
			
			val lines =
				string
					.split("\n").nn.map(_.nn)
					.map(line => "  " * level + line)
			
			val lastLine = lines.last
			
			lines
				.dropRight(1)
				.map(_ + "\n")
				.appended(lastLine)
				.reduce(_+_)
		}
	}
	
	
	def prettyPrint(value: Any): String = {
		
		value match {
			case value: String =>
				s"\"$value\""
				
			case Nil => "[]"
			case values: Iterable[?] =>
				val lastValue = values.last
				
				var result = "["
				
				values.dropRight(1).foreach { value =>
					result = result + prettyPrint(value) + ", "
				}
				
				result = result + prettyPrint(lastValue)
				
				result + "]"
				
			case value: Product =>
				
				val typeName = value.productPrefix
				
				
				var result = s"$typeName("
				
				val fields = value.productElementNames.zip(value.productIterator).toList
				if (fields.nonEmpty) {
					fields.dropRight(1).foreach { case (name, value) =>
						result = result + s"$name=${prettyPrint(value)}, "
					}
					
					val (lastName, lastValue) = fields.last
					result = result + s"$lastName=${prettyPrint(lastValue)}"
				}
				
				result + ")"
				
			case _ => value.toString
		}
	}
}
