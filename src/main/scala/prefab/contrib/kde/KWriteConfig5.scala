package prefab.contrib.kde

import prefab.prelude.plan.*

package object kwriteconfig5 {
	export prefab.contrib.kde.Group
	
	case class KWriteConfig5(
		file: File,
		group: Group,
		configs: (String, String)*
	) extends Plan {
		
		def ensure(host: LocalHost) = {
			file.logIfChanged {
				configs.foreach { case (key, value) =>
					host.ensure(
						Command(s"""kwriteconfig5 --file "$file" --group "$group" --key "$key" "$value"""")
					)
				}
			}
		}
	}
}
