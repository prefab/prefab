package prefab.contrib.kde

import prefab.prelude.plan.*

package object kreadconfig5 {
	export prefab.contrib.kde.Group

	case class KReadConfig5(
		file: File,
		group: Group,
		key: String,
	) extends Check[String] {
		
		def check(host: LocalHost): String = {
			val command = s"""kreadconfig5 --file "$file" --group "$group" --key "$key""""
			
			host.check(
				Command(command)
			) match {
				case Command.Ok(_, stdout, _) =>
					stdout.mkString("\n")
					
				case Command.Error(_, _, _, stderr) =>
					host.fail(
						s""""$command" failed: $stderr"""
					)
			}
		}
	}
}
