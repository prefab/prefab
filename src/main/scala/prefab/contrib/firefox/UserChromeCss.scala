package prefab.contrib.firefox

import prefab.prelude.plan.*


package object user_chrome_css {
	
	case class UserChromeCss(
		userDir: Dir,
		css: String,
	) extends Plan {
		
		def ensure(host: LocalHost) = {
			ProfileDirs.list(host, userDir)
				.map(_.dir("chrome"))
				.map(_.file("userChrome.css"))
				.foreach { userChromeFile =>
					
					val namespaceLine = """@namespace url("http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul");"""
					
					val templateText = if (css.startsWith(namespaceLine) == false) {
						namespaceLine + System.lineSeparator + css
					} else {
						css
					}
					
					userChromeFile.ensure(_.textIs(templateText))
				}
		}
	}
}
