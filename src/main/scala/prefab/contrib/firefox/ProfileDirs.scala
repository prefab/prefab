package prefab.contrib.firefox

import prefab.filesystem.Dir
import prefab.core.host.LocalHost


object ProfileDirs {
	private val profileDirNamePattern = """[a-z0-9]{8}\..*"""
	
	def list(host: LocalHost, userDir: Dir): Seq[Dir] = {
		val firefoxDir = userDir.dir(".mozilla/firefox")
		
		firefoxDir.listDirs()
			.filter(_.name.matches(profileDirNamePattern))
	}
}
