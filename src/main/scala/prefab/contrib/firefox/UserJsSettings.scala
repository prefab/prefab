package prefab.contrib.firefox

import prefab.prelude.plan.*


package object user_js_settings {
	
	/**
	* Change Firefox settings like you would set them in about:config.
	* 
	* To do so, this creates a user.js-file in your Firefox Profile directory.
	* (If you have multiple Firefox Profiles, it will create a user.js file in every Profile directory.)
	*/
	case class UserJsSettings(
		userDir: Dir,
		configs: (String, String|Int|Boolean)*
	) extends Plan {
		
		def ensure(host: LocalHost) = {
			ProfileDirs.list(host, userDir)
			.map(_.file("user.js"))
			.foreach { userJsFile =>
				
				val lines = configs.map { case (key, value) =>
					value match {
						case _: String =>
							s"""user_pref("$key", "$value");"""
						case _: Int | _: Boolean =>
							s"""user_pref("$key", $value);"""
					}
				}
				
				userJsFile.ensure(_.textIs(lines.mkString("\n")))
			}
		}
	}
}
