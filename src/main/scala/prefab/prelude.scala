package prefab.prelude


package object config {
	export prefab.core.Deployment
	export prefab.core.deploy
	export prefab.core.host.RemoteHost
	export prefab.filesystem.{File, Dir, Path}
}


package object plan {
	export prefab.core.action.{Check, Plan}
	export prefab.core.host.{LocalHost, RemoteHost}
	export prefab.filesystem.{File, Dir, Path}
	export prefab.tech.command.Command
}


package object test {
	export prefab.filesystem.{File, Dir, Path}
}

