package prefab.tech.command

import scala.sys.process._

import prefab.prelude.plan.*
import prefab.core.host.LocalHost


case class Command(command: String) extends Plan with Check[Command.Result] {
	
	var errorHandlingEnabled = false
	
	def check(host: LocalHost): Command.Result = {
		executeCommand(host, command, errorHandlingEnabled)
	}
	def ensure(host: LocalHost) = {
		executeCommand(host, command, errorHandlingEnabled)
	}
	
	def handleErrors(): Command = {
		this.errorHandlingEnabled = true
		this
	}
}


object Command {
	sealed trait Result {
		override def toString: String = ResultFormat.format(this)
	}
	case class Ok(command: String, stdout: Seq[String], stderr: Seq[String]) extends Result
	case class Error(command: String, exitCode: Int, stdout: Seq[String], stderr: Seq[String]) extends Result
}


private def executeCommand(host: LocalHost, command: String, errorHandlingEnabled: Boolean): Command.Result = {
	
	var stdout = Seq.empty[String]
	var stderr = Seq.empty[String]
	val processLogger = ProcessLogger(
		line => stdout = stdout :+ line,
		line => stderr = stderr :+ line,
	)
	
	val exitCode = Process(command).!<(processLogger)
	
	if (exitCode == 0) {
		Command.Ok(command, stdout, stderr)
	}
	else {
		val result = Command.Error(command, exitCode, stdout, stderr)
		
		if (errorHandlingEnabled == false) {
			host.fail(result.toString)
		}
		result
	}
}
