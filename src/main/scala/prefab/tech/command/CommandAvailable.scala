package prefab.tech.command

import prefab.prelude.plan.*


case class CommandAvailable(
	commandName: String,
) extends Check.WithCache[Boolean] {
	
	def check(host: LocalHost): Boolean = {
		host.check(Command(s"which $commandName").handleErrors())
			.isInstanceOf[Command.Ok]
	}
}
