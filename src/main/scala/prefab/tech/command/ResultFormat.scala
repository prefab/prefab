package prefab.tech.command


private[command] object ResultFormat {
	
	def format(result: Command.Result): String = result match {
		
		case Command.Ok(command, stdout, stderr) => {
			
			var string =
				s""">Command.Result.Ok:
				    >""".stripMargin('>')
			
			if (stdout.nonEmpty) {
				string +=
					s""">  stdout:
					    >${formatOutput(stdout, 2)}
					    >""".stripMargin('>')
			}
			
			if (stderr.nonEmpty) {
				string +=
					s""">  stderr:
					    >${formatOutput(stderr, 2)}
					    >""".stripMargin('>')
			}
			
			string
		}
		
		
		case Command.Error(command, exitCode, stdout, stderr) => {
			
			var string =
				s""">Command.Result.Error:
				    >  exitCode: $exitCode
				    >""".stripMargin('>')
			
			if (stdout.nonEmpty) {
				string +=
					s""">  stdout:
					    >${formatOutput(stdout, 2)}
					    >""".stripMargin('>')
			}
			
			string +=
				s""">  stderr:
				    >${formatOutput(stderr, 2)}
				    >""".stripMargin('>')
			
			string
		}
	}
	
	
	private def formatOutput(result: Seq[String], indent: Int): String = {
		val indentLevel = indent-1
		val indentation = "  " * indentLevel + "| "
		
		indentation + result.mkString("\n" + indentation)
	}
}
