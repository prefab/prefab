package prefab.tech.pkg

import prefab.prelude.plan.*


object Zypper extends PackageManager {
	
	def install(host: LocalHost, packages: Seq[String]): Unit = {
		host.ensure(
			Command(s"zypper install --no-confirm ${packages.mkString(" ")}")
		)
	}
}
