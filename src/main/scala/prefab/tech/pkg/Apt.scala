package prefab.tech.pkg

import prefab.prelude.plan.*


object Apt extends PackageManager {
	
	def install(host: LocalHost, packages: Seq[String]): Unit = {
		host.ensure(
			Command(s"apt-get --assume-yes install ${packages.mkString(" ")}")
		)
	}
}
