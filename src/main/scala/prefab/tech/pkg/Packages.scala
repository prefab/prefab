package prefab.tech.pkg

import prefab.prelude.plan.*
import prefab.tech.command.CommandAvailable


package object packages {
	
	object Packages {
		case class Installed(
			packages: String*
		) extends Plan {
			
			def ensure(host: LocalHost) = {
				
				val packageManager = host.check(AvailablePackageManager)
				packageManager.install(host, packages)
			}
		}
	}
	
	
	case object AvailablePackageManager extends Check.WithCache[PackageManager] {
		
		def check(host: LocalHost): PackageManager = {
			if (host.check(CommandAvailable("zypper"))) {
				Zypper
			}
			else if (host.check(CommandAvailable("apt-get"))) {
				Apt
			}
			else {
				host.fail("No supported Package Manager found.")
			}
		}
	}
}
