package prefab.tech.pkg

import prefab.core.host.LocalHost


trait PackageManager {
	
	def install(host: LocalHost, packages: Seq[String]): Unit
}
