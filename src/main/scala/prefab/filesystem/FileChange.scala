package prefab.filesystem

import java.nio.charset.StandardCharsets
import java.nio.file.{Files, StandardOpenOption}


private class FileChange private[filesystem] (file: File) {
	
	def containsLine(line: String): FileChange = {
		if (this.file.exists == false) {
			if (line.endsWith(System.lineSeparator)) {
				write(line, append=false)
			} else {
				write(line + System.lineSeparator, append=false)
			}
		}
		else if (this.file.lines.contains(line) == false) {
			appendLine(line)
		}
		this
	}
	
	
	def startsWithLine(line: String): FileChange = {
		if (this.file.exists == false) {
			if (line.endsWith(System.lineSeparator)) {
				write(line, append=false)
			} else {
				write(line + System.lineSeparator, append=false)
			}
		}
		else if (this.file.lines.next != line) {
			prependLine(line)
		}
		this
	}
	
	
	/**
	 * Set the text in a file to the given `text`:
	 * ```
	 * file.ensure(_.textIs("Hello World")) //after this, the file content will be "Hello World"
	 * ```
	 */
	def textIs(text: String): FileChange = {
		if (this.file.exists == false || this.file.text != text) {
			Files.createDirectories(this.file.parent.raw)
			
			write(text, append=false)
		}
		this
	}
	
	
	private def append(text: String) = write(text, append=true)
	
	private def appendLine(line: String) =
		if (this.file.text.endsWith(System.lineSeparator)) {
			append(line + System.lineSeparator)		
		} else {
			append(System.lineSeparator.nn + line + System.lineSeparator.nn)
		}
	
	
	private def prepend(text: String) = {
		val fileContent = text + this.file.text
		
		write(fileContent, append=false)
	}
	
	private def prependLine(line: String) = {
		prepend(line + System.lineSeparator)
	}
	
	
	private def write(text: String, append: Boolean) = {
		Files.createDirectories(file.raw.getParent)
		
		Files.write(
			file.raw,
			text.getBytes(StandardCharsets.UTF_8),
			StandardOpenOption.CREATE,
			if (append) StandardOpenOption.APPEND else StandardOpenOption.TRUNCATE_EXISTING,
		)
	}
}
