package prefab.filesystem

import java.nio.file.Files


private class DirChange private[filesystem] (dir: Dir) {
	
	def exists(): DirChange = {
		if (Files.exists(dir.raw) == false) {
			Files.createDirectories(dir.raw)
		}
		this
	}
}
