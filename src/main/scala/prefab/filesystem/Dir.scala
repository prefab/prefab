package prefab.filesystem

import java.util.Arrays
import java.nio.file.Files

import scala.jdk.StreamConverters.*

import prefab.core.host.LocalHost


case class Dir private[prefab] (host: LocalHost, raw: java.nio.file.Path) extends Path(host, raw) {
	
	def ensure(code: DirChange => Unit): Unit = {
		code(DirChange(this))
	}
	
	def list(): Seq[Path] = {
		
		val paths = Files.list(this.raw).nn.toScala(Seq)
		
		paths.map { path =>
			if (Files.isDirectory(path)) {
				Dir(this.host, path)
			} else {
				File(this.host, path)
			}
		}
	}
	
	def listDirs(): Seq[Dir] = {
		this.list().flatMap {
			case dir: Dir => Some(dir)
			case _: File => None
		}
	}
	
	def listFiles(): Seq[File] = {
		this.list().flatMap {
			case file: File => Some(file)
			case _: Dir => None
		}
	}
	
	
	def dir(name: String): Dir = {
		val dir = this.raw.resolve(name).nn
		Dir(this.host, dir)
	}
	
	def file(name: String): File = {
		val file = this.raw.resolve(name).nn
		File(this.host, file)
	}
}
