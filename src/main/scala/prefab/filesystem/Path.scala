package prefab.filesystem

import prefab.core.host.LocalHost


class Path(host: LocalHost, raw: java.nio.file.Path) {
	
	def name: String = this.raw.getFileName.nn.toString
	
	
	def parent: Dir = {
		val parentDir = this.raw.getParent.nn
		Dir(this.host, parentDir)
	}
	
	
	override def toString = raw.toString
}
