package prefab.filesystem

import java.io.FileNotFoundException
import java.nio.file.Files
import scala.util.{Try, Success, Failure}

import scala.io.Source
import scala.io.Codec

import prefab.core.host.LocalHost
import prefab.core.Log

given Codec = Codec.UTF8


class File private[prefab] (host: LocalHost, private[prefab] val raw: java.nio.file.Path) extends Path(host, raw) {
	
	def ensure(code: FileChange => Unit): Unit = logIfChanged {
		code(FileChange(this))
	}
	
	
	/**
	 * Monitor external changes made to this file, e.g. from a command, and include them in the output when they happen.
	 * ```
	 * file.logIfChanged {
	 *	 host.change(Command(s"rm $file"))
	 * }
	 * ```
	 */
	def logIfChanged(code: => Unit): Unit = {
		
		val beforeState = File.State.of(this)
		code
		val afterState = File.State.of(this)
		
		(beforeState -> afterState) match {
			case (File.State.ExistsNot -> File.State.ExistsNot) => //do nothing
			case (File.State.Exists(_) -> File.State.ExistsNot) => host.logRaw(Log.Entry.info(s"File was deleted: ${this.raw.toAbsolutePath()}"))
			case (File.State.ExistsNot -> File.State.Exists(_)) => host.logRaw(Log.Entry.info(s"File was created: ${this.raw.toAbsolutePath()}"))
			case (File.State.Exists(before) -> File.State.Exists(after)) => if (before != after) {
				host.logRaw(Log.Entry.info(s"File was modified: ${this.raw.toAbsolutePath()}"))
			}
		}
	}
	
	
	def exists: Boolean = Files.exists(this.raw)
	
	
	def bytes: Array[Byte] = {
		Try(Files.readAllBytes(this.raw).nn)
			.transform(
				value => Success[Array[Byte]](value),
				throwable => Failure[Array[Byte]](FileNotFoundException(throwable.getMessage))
			).get
	}
	
	def text: String = Source.fromFile(this.raw.toFile.nn).mkString
	
	def lines: Iterator[String] = Source.fromFile(this.raw.toFile.nn).getLines
	
	
	/**Calculates the MD5 checksum for this file's content.*/
	def md5: Checksum.Md5 = Checksum.md5(this)
	
	
	/**Returns whether this file has changed throughout the deployment.*/
	private[prefab] def hasChanged: Boolean = this.initialState != File.State.of(this) //TODO consider removing
	
	private[prefab] def calculateChange(): File.State.Change = File.State.Change(this.initialState, File.State.of(this))
	
	
	
	private val initialState: File.State = File.State.of(this)
	
	host.registerFile(this)
}

object File {
	private[prefab] enum State {
		case Exists(checksum: Checksum)
		case ExistsNot
	}
	private[prefab] object State {
		def of(file: File) = if (file.exists) { Exists(file.md5) } else { ExistsNot }
		
		case class Change(before: State, after: State)
	}
}
