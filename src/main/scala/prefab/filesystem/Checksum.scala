package prefab.filesystem

import java.security.MessageDigest


object Checksum {
	def md5(file: File) = Checksum.Md5(calculate("MD5", file))
	
	private def calculate(algorithm: String, file: File): Array[Byte] = {
		val messageDigest = MessageDigest.getInstance(algorithm).nn
		messageDigest.update(file.bytes)
		messageDigest.digest().nn
	}
	
	case class Md5(bytes: Array[Byte]) extends Checksum(bytes)
}

trait Checksum(private val bytes: Array[Byte]) {
	lazy val hexString: String = bytes.map("%02x".format(_)).mkString
	
	override def equals(other: Any) = other match {
		case other: Checksum => this.bytes.sameElements(other.bytes)
		case _ => false
	}
}
