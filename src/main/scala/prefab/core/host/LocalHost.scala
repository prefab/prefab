package prefab.core.host

import scala.collection.mutable

import prefab.core.Log
import prefab.core.action.{Check, Plan}
import prefab.filesystem.{File, Dir}
import prefab.util.Formatting.indent


case class LocalHost private[prefab] (userDirPath: java.nio.file.Path, private val plans: Set[Plan]) {

	private var logs: Log.List = Log.List.empty(indent=0)
	private val checkResultCache = CheckResultCache()
	private var files: Seq[File] = Seq.empty


	def check[T](check: Check.WithCache[T]): T = {

		checkResultCache.load(check) match {
			case Some(result) =>
				this.logRaw(Log.Entry.check(check))
				this.deriveLoggingContext { host =>
					host.logRaw(Log.Entry.result(cached=true, result))
				}
				result

			case None =>
				val result = this.check(check.asInstanceOf[Check[T]])
				checkResultCache.store(check, result)
				result
		}
	}

	def check[T](check: Check[T]): T = {

		this.logRaw(Log.Entry.check(check))

		this.deriveLoggingContext { host =>
			val result = check.check(host)
			host.logRaw(Log.Entry.result(cached=false, result))
			result
		}
	}

	def ensure(plan: Plan): Unit = {

		this.logRaw(Log.Entry.plan(plan))

		this.deriveLoggingContext { host =>
			val result = plan.ensure(host)
			if (result != ()) {
				host.logRaw(Log.Entry.result(cached=false, result))
			}
			result
		}
	}


	def dir(path: String): Dir = Dir(this, javaPath(path))

	val userDir: Dir = Dir(this, userDirPath)

	val tempDir: Dir = Dir(this, javaPath(System.getProperty("java.io.tmpdir").nn))


	def file(path: String): File = File(this, javaPath(path))


	inline def fail(message: String): Nothing = {

		println("\n")
		this.logRaw(Log.Entry.info("Deployment failed with message:\n\n" + message.indent(1)))
		println()

		val stackTrace = Throwable().getStackTrace().nn
			.map(_.toString)
			.filterNot { line =>
				line.startsWith("scala.runtime.") ||
				line.startsWith("java.base/") ||
				line.startsWith("sbt.")
			}
			.mkString("\n")

		this.logRaw(Log.Entry.info(s"Failure occurred\n$stackTrace"))

		scala.sys.exit(1)
	}


	def log(message: String): Unit = {
		this.logRaw(Log.Entry.info(message))
	}


	private[prefab] def logRaw(entry: Log.Entry): Unit = {
		this.logs = this.logs.appended(entry)
	}


	private[prefab] def runDeployment() = {
		this.plans.foreach { plan =>
			this.logRaw(Log.Entry.plan(plan))
			this.deriveLoggingContext { host =>
				plan.ensure(host)
			}
		}
	}

	private[prefab] def registerFile(file: File): Unit = {
		this.files = this.files.appended(file)
	}
	private[prefab] def registeredFiles: Seq[File] = this.files


	private inline def deriveLoggingContext[T](code: LocalHost => T): T = {
		val parentLogs = this.logs
		this.logs = Log.List.empty(this.logs.indent + 1)

		val result = code(this)

		val childLogs = this.logs
		this.logs = parentLogs.appended(childLogs)

		result
	}

	private def javaPath(path: String) = java.nio.file.Paths.get(path).nn
}
