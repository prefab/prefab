package prefab.core.host

import scala.collection.mutable.HashMap

import prefab.core.action.Check


private[host] class CheckResultCache(cache: HashMap[Check.WithCache[_], Any] = HashMap.empty) {
	
	private[host] def load[T](check: Check.WithCache[T]): Option[T] = {
		cache.get(check).map(_.asInstanceOf[T])
	}
	
	private[host] def store[T](check: Check.WithCache[T], result: T): Unit = {
		cache.put(check, result)
	}
}
