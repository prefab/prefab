package prefab.core.host

import scala.collection.mutable
import prefab.core.action.Plan


object RemoteHost {
	private[prefab] def apply(hostname: String, userDir: java.nio.file.Path): RemoteHost = new RemoteHost(hostname, userDir, mutable.Set.empty)
}

case class RemoteHost private (hostname: String, userDir: java.nio.file.Path, var plans: mutable.Set[Plan]) {

	def assign(plan: Plan) = {
		this.plans.addOne(plan)
	}


	private[prefab] def runDeployment() = {
		LocalHost(userDir, this.plans.toSet).runDeployment()
	}
}
