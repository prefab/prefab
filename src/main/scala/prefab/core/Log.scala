package prefab.core

import java.time.Instant
import java.time.temporal.ChronoUnit

import scala.collection.mutable

import prefab.core.action.{Check, Plan}

import prefab.util.Formatting
import prefab.util.Formatting.indent


sealed trait Log
object Log {
	
	case class Entry(timestamp: Instant, content: Plan | Check[?] | Entry.Result | Entry.Info) extends Log {
	
		val display = content match {
			case plan: Plan =>
				val formatted = Formatting.prettyPrint(plan)
				s"[Plan] $formatted"
			case check: Check[?] =>
				val formatted = Formatting.prettyPrint(check)
				s"[Check] $formatted"
			case Entry.Result(cached, value) =>
				val formatted = Formatting.prettyPrint(value)
				if (cached) {
					s"[Result][Cache] $formatted"
				} else {
					s"[Result] $formatted"
				}
			case Entry.Info(message) =>
				val timestampFormatted = timestamp.truncatedTo(ChronoUnit.MILLIS)
				s"[$timestampFormatted] $message"
		}
	}
	
	object Entry {
		def plan(plan: Plan) = Log.Entry(timestamp=Instant.now.nn, content=plan)
		def check(check: Check[?]) = Log.Entry(timestamp=Instant.now.nn, content=check)
		
		case class Result(cached: Boolean, value: Any)
		def result(cached: Boolean, value: Any) = Log.Entry(timestamp=Instant.now.nn, content=Log.Entry.Result(cached, value))
		
		case class Info(message: String)
		def info(message: String) = Log.Entry(timestamp=Instant.now.nn, Log.Entry.Info(message))
	}


	object List {
		def empty(indent: Int) = Log.List(logs = mutable.ListBuffer.empty, indent)
	}
	case class List private(logs: mutable.ListBuffer[Log], indent: Int) extends Log {
		
		private[prefab] def appended(logList: Log.List): Log.List = {
			this.logs.append(logList)
			this
		}
		
		private[prefab] def appended(logEntry: Log.Entry): Log.List = {
			this.logs.append(logEntry)
			
			if (indent == 0) {
				println()
			}
			println(logEntry.display.indent(indent))
			
			this
		}
	}
}
