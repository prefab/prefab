package prefab.core.action

import prefab.core.host.LocalHost


trait Check[T] {
	def check(host: LocalHost): T
}

object Check {
	trait WithCache[T] extends Check[T] {}
}
