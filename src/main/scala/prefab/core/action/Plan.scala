package prefab.core.action

import prefab.core.host.LocalHost


trait Plan {
	def ensure(host: LocalHost): Unit
}
