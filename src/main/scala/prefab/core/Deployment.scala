package prefab.core

import prefab.core.host.RemoteHost
import prefab.util.Formatting


def deploy(code: (Deployment) => Unit) = {
	def uid = new com.sun.security.auth.module.UnixSystem().getUid()
	if (uid != 0) {
		println("Need to run as root. Exiting.")
		System.exit(1)
	}

	val deployment = Deployment()

	code(deployment)

	deployment.start()
}


object Deployment {
	def apply(): Deployment = Deployment(hosts = Set.empty)
}

case class Deployment private (var hosts: Set[RemoteHost]) {

	def host(hostname: String, userDir: java.nio.file.Path): RemoteHost = {
		val host = RemoteHost(hostname, userDir)
		this.hosts = this.hosts + host
		host
	}

	private[prefab] def start() = {
		import scala.sys.process.*
		val hostname = Process("hostname").!!.trim()

		this.hosts.find(_.hostname == hostname) match {
			case None =>
				println(s"""ERROR: Current host with hostname "$hostname" is not configured in deployment. Aborting.""")

			case Some(localhost) =>
				val plansString = localhost.plans.foldLeft("") { (string, plan) =>
					string + s" - ${Formatting.prettyPrint(plan)}\n"
				}
				println(s"""Running on host with hostname "${localhost.hostname}" and assigned plans:\n$plansString""")
				localhost.runDeployment()
		}
	}
}
