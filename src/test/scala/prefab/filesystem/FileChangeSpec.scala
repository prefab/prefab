package prefab.filesystem

import java.nio.file.Files

import scala.io.Source
import scala.io.Codec

import prefab.prelude.test.*
import prefab.prelude.plan.*

given Codec = Codec.UTF8


class FileChangeSpec extends prefab.Spec {
	
	"The FileChange-API" - {
		"should ensure a file contains an exact text" in deployment { host =>
			
			val testee = host.tempDir.file("prefab-test-write-text.txt")
			testee.exists shouldBe false
			
			testee.ensure(_.textIs("body"))
			
			testee.exists shouldBe true
			testee.text shouldBe "body"
			
			testee.ensure(_.textIs("body2"))
			
			testee.text shouldBe "body2"
		}
		"should ensure a file contains a line of text, appending to the end, if it does not" in deployment { host =>
			
			val testee = host.tempDir.file("prefab-test-append-text.txt")
			testee.exists shouldBe false
			
			testee.ensure(_.containsLine("footer"))
			
			testee.exists shouldBe true
			testee.text shouldBe "footer\n"
			
			testee.ensure(_.textIs("body"))
			testee.ensure(_.containsLine("footer"))
			
			testee.text shouldBe "body\nfooter\n"
			
			testee.ensure(_.containsLine("footer"))
			
			testee.text shouldBe "body\nfooter\n"
			
			testee.ensure(_.textIs("header\nbody"))
			testee.ensure(_.containsLine("header"))
			
			testee.text shouldBe "header\nbody"
		}
		"should ensure a file starts with a line of text, prepending to the start, if it does not" in deployment { host =>
			
			val testee = host.tempDir.file("prefab-test-prepend-text.txt")
			testee.exists shouldBe false
			
			testee.ensure(_.startsWithLine("header"))
			
			testee.exists shouldBe true
			testee.text shouldBe "header\n"
			
			testee.ensure(_.textIs("body"))
			testee.ensure(_.startsWithLine("header"))
			
			testee.text shouldBe "header\nbody"
			
			testee.ensure(_.startsWithLine("header"))
			
			testee.text shouldBe "header\nbody"
		}
		"should write non-ASCII characters via UTF-8" in deployment { host =>
			
			val testee = host.tempDir.file("prefab-test-utf8.txt")
			
			testee.ensure(_.textIs("🏳‍🌈"))
			
			Source.fromFile(testee.raw.toFile.nn).mkString shouldBe "🏳‍🌈"
			
			Files.readAllBytes(testee.raw).nn.map(_ & 0xff) shouldBe Array(
				0xF0, 0x9F, 0x8F, 0xB3, //🏳‍ (U+1F3F3)
				0xE2, 0x80, 0x8D, //ZERO WIDTH JOINER (U+200D)
				0xF0, 0x9F, 0x8C, 0x88, //🌈 (U+1F308)
			)
		}
	}
}
