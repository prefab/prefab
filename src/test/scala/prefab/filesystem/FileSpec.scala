package prefab.filesystem

import java.nio.file.Files
import java.io.FileNotFoundException

import prefab.prelude.test.*
import prefab.prelude.plan.*


class FileSpec extends prefab.Spec {
	
	"The File-API" - {
		
		"should read the bytes of a file" in deployment { host =>
			val javaFile = Files.createTempFile("prefab-test-read-bytes", ".txt")
			val testee = host.file(javaFile.toString)
			
			testee.bytes shouldBe Array[Byte]()
			
			Files.delete(javaFile)
			
			assertThrows[FileNotFoundException](testee.bytes)
		}
		
		"should read the text in a file" in deployment { host =>
			val javaFile = Files.createTempFile("prefab-test-read-text", ".txt")
			val testee = host.file(javaFile.toString)
			
			testee.lines.toList shouldBe List()
			
			Files.delete(javaFile)
			
			assertThrows[FileNotFoundException](testee.lines)
		}
		
		"should read the lines of text in a file" in deployment { host =>
			val javaFile = Files.createTempFile("prefab-test-read-text", ".txt")
			val testee = host.file(javaFile.toString)
			
			testee.text shouldBe ""
			
			Files.delete(javaFile)
			
			assertThrows[FileNotFoundException](testee.text)
		}
		
		"should read non-ASCII characters via UTF-8" in deployment { host =>
			
			val resource = getClass.getResource("utf8.txt").nn
			val testee = host.file(resource.getPath.toString)
			
			testee.text shouldBe "🏳‍🌈\n"
			testee.lines.toSeq shouldBe Seq("🏳‍🌈")
		}
		
		
		"should calculate the checksum" in deployment { host =>
			
			val resource = getClass.getResource("utf8.txt").nn
			val testee = host.file(resource.getPath.toString)
			
			testee.md5.hexString shouldBe "d0c51c1520c943726ffaff3e3e60ed32"
		}
		
		
		"should mark a change" in deployment { host =>
			
			val javaFile = Files.createTempFile("prefab-test-mark-change", ".txt")
			
			{
				val testee = host.file(javaFile.toString)
				testee.ensure { _ =>
					//do nothing
				}
				testee.hasChanged shouldBe false
			}
			
			{
				val testee = host.file(javaFile.toString)
				testee.ensure { _ =>
					Files.write(javaFile, Array[Byte](1, 2, 3))
				}
				testee.hasChanged shouldBe true
			}
			
			{
				val testee = host.file(javaFile.toString)
				testee.ensure { _ =>
					Files.delete(javaFile)
				}
				testee.hasChanged shouldBe true
			}
			
			{
				val testee = host.file(javaFile.toString)
				testee.ensure { _ =>
					Files.createFile(javaFile)
				}
				testee.hasChanged shouldBe true
			}
			
			Files.delete(javaFile)
		}
	}
}
