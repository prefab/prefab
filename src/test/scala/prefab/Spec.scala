package prefab

import java.nio.file.Files

import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

import prefab.core.host.LocalHost
import prefab.filesystem.File


abstract class Spec extends AnyFreeSpec with Matchers {

	def deployment(testCode: LocalHost => Unit) = deploymentWithUserDir("/tmp/home-prefab-test")(testCode)

	def deploymentWithUserDir(userDir: String)(testCode: LocalHost => Unit) = {
		val localhost = LocalHost(java.nio.file.Paths.get(userDir).nn, Set.empty)

		try {
			testCode(localhost)

			localhost.runDeployment()
		}
		finally {
			localhost.registeredFiles
				.map(file => (file, file.calculateChange()))
				.foreach {
					case (file, File.State.Change(File.State.ExistsNot, File.State.Exists(_))) =>
						Files.delete(file.raw) //TODO introduce own file deletion API and use that

					case _ => //do nothing
				}
		}
	}
}
