package prefab.util

import Formatting.indent


class FormattingSpec extends prefab.Spec {
	
	"Formatting" - {
		"should indent to the given level" in {
			
			"".indent(1) shouldBe "  "
			
			"Foo".indent(1) shouldBe "  Foo"
			
			"Foo\nBar\nBaz".indent(1) shouldBe "  Foo\n  Bar\n  Baz"
		}
		
		
		"should pretty-print" - {
			
			"an empty list" in {
				
				val result = Formatting.prettyPrint(Seq())
				
				result shouldBe "[]"
			}
			
			"complex case classes" in {
				
				case class Lorem(ipsum: Dolor, adipiscing: Float)
				case class Dolor(sit: Seq[String])
				
				val testee = Lorem(
					ipsum=Dolor(
						sit=Seq("amet", "consectetur")
					),
					adipiscing=4.2
				)
				
				val result = Formatting.prettyPrint(testee)
				
				result shouldBe """Lorem(ipsum=Dolor(sit=["amet", "consectetur"]), adipiscing=4.2)"""
			}
		}
	}
}
