package prefab.core.host

import prefab.prelude.test.*
import prefab.prelude.plan.*


class CheckResultCacheSpec extends prefab.Spec {
	
	class Fixture {
		val testee = CheckResultCache()
		
		val check1 = ExampleCheck("1")
		val check2 = ExampleCheck("2")
	}
	
	case class ExampleCheck(name: String) extends Check.WithCache[String] {
		def check(host: LocalHost): String = {
			s"Hello $name!"
		}
	}
	
	
	"A CheckResultCache" - {
		"when empty should return None" in new Fixture { fixture =>
			import fixture.*
			
			testee.load(check1) shouldBe None
		}
		
		"when filled" - {
			"should return a check result" in new Fixture { fixture =>
				import fixture.*
				
				deployment { host =>
					val check1Result = check1.check(host)
					
					testee.store(check1, check1Result)
					
					testee.load(check1) shouldBe Some(check1Result)
				}
			}
			
			"should return the correct check result" in new Fixture { fixture =>
				import fixture.*
				
				deployment { host =>
					val check1Result = check1.check(host)
					val check2Result = check2.check(host)
					
					testee.store(check1, check1Result)
					testee.store(check2, check2Result)
					
					testee.load(check1) shouldBe Some(check1Result)
					testee.load(check2) shouldBe Some(check2Result)
					check1Result should not be check2Result
				}
			}
		}
	}
}
