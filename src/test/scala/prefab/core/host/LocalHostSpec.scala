package prefab.core.host


class LocalHostSpec extends prefab.Spec {
	"A LocalHost" - {

		val userDir = "/home/user"

		"should keep track of the created, opened and deleted files" in deploymentWithUserDir(userDir) { host =>
			host.userDir.file(".profile")
			host.tempDir.file("prefab-test.txt")
			host.dir("/root") //not a file, should get ignored

			val result = host.registeredFiles

			result should have size 2
			result.head.toString shouldBe s"$userDir/.profile"
			result.tail.head.toString shouldBe "/tmp/prefab-test.txt"
		}
	}
}
