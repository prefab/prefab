
ThisBuild / scalaVersion := "3.2.1"
ThisBuild / organization := "io.prefab"


libraryDependencies += "org.scalatest" %% "scalatest-freespec" % "3.2.15" % "test"
libraryDependencies += "org.scalatest" %% "scalatest-shouldmatchers" % "3.2.15" % "test"


scalacOptions ++= Seq(
	"-Yexplicit-nulls",
	"-no-indent",
)

